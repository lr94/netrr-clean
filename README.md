Assuming that the HB source and target datasets are located in `./datasets/HB`, the model can be trained with

```
./train_rr.py \
	--source synHB \
	--target valHB \
	--epoch 40 \
	--batch_size 64 \
	--lr 0.0003 \
	--weight_rot 1 \
	--weight_ent 0.1
```

The saved models can be evaluated on target with
```
./eval.py \
	--source synHB\
	--target valHB \
	--epoch 40 \
	--batch_size 64 \
	--lr 0.0003 \
	--weight_rot 1 \
	--weight_ent 0.1
```

To select a different location for the dataset, the following options can be used:
```
--data_root_source
--data_root_target
--train_file_source
--test_file_source
--train_file_target
--test_file_target
```